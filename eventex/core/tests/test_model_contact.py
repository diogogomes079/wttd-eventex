from django.test import TestCase
from django.core.exceptions import ValidationError
from eventex.core.models import Speaker, Contact


class ContactModelTest(TestCase):
    def setUp(self):
        self.speaker = Speaker.objects.create(
            name="Grace Hopper",
            slug='grace-hopper',
            photo="http://hbn.link/hopper-pic",
        )

    def test_email(self):
        contact = Contact.objects.create(speaker=self.speaker, kind=Contact.EMAIL, value='grace@hopper.com')

        self.assertTrue(Contact.objects.exists())

    def test_phone(self):
        contact = Contact.objects.create(speaker=self.speaker, kind=Contact.PHONE, value='47-22346-7737')

        self.assertTrue(Contact.objects.exists())

    def test_choices(self):
        '''Contact kind should be limited to E or P'''
        contact = Contact(speaker=self.speaker,kind='A', value='B')
        self.assertRaises(ValidationError, contact.full_clean)

    def test_str(self):
        contact = Contact(speaker=self.speaker, kind=Contact.EMAIL, value='diogogomes079@gmail.com')
        self.assertEqual('diogogomes079@gmail.com', str(contact))

class ContactManageTest(TestCase):
    def setUp(self):
        s = Speaker.objects.create(
            name='Diogo Gomes',
            slug='diogo-gomes',
            photo='https://avatars0.githubusercontent.com/u/15928598?v=3&s=466'
        )
        s.contact_set.create(kind=Contact.EMAIL, value='diogogomes079@gmail.com')
        s.contact_set.create(kind=Contact.PHONE, value='85-12345-6789')

    def test_emails(self):
        qs = Contact.objects.emails()
        expected = ['diogogomes079@gmail.com']
        self.assertQuerysetEqual(qs, expected, lambda o: o.value)

    def test_phones(self):
        qs = Contact.objects.phones()
        expected = ['85-12345-6789']
        self.assertQuerysetEqual(qs, expected, lambda o: o.value)
