from django.core import mail
from django.test import TestCase
from django.shortcuts import resolve_url as r


class SubscribePostValid(TestCase):
    def setUp(self):
        data = dict(name='Diogo Gomes', cpf='12345678901',
                    email='diogogomes079@gmail.com', phone='85-12345-6789')
        self.client.post(r('subscriptions:new'), data)
        self.email = mail.outbox[0]

    def test_subscription_email_subject(self):
        expect = 'Confirmação de Inscrição'

        self.assertEqual(expect, self.email.subject)

    def test_subcription_email_from(self):
        expect = 'contato@eventex.com.br'

        self.assertEqual(expect, self.email.from_email)

    def test_subcription_email_to(self):
        expect = ['contato@eventex.com.br', 'diogogomes079@gmail.com']

        self.assertEqual(expect, self.email.to)

    def test_subscription_email_body(self):
        contents = [
            'Diogo Gomes',
            '12345678901',
            'diogogomes079@gmail.com',
            '85-12345-6789'
        ]

        for content in contents:
            with self.subTest():
                self.assertIn(content, self.email.body)
